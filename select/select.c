/*
 * select.c
 *
 *  Created on: 2014-8-22
 *      Author: hzcaoyanming
 */
#include "select.h"
void Select(int arr[], int arrLength)
{
	int i = 0;
	while(i<arrLength)
	{
		int sign =i;
		int j = i+1;
		while(j<arrLength)
		{
			if(arr[j]<arr[sign])
			{
				sign = j;
			}
			j++;
		}
		int temp = arr[i];
		arr[i] = arr[sign];
		arr[sign] = temp;
		i++;
	}
}
