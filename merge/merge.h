/*
 * merge.h
 *
 *  Created on: 2014-8-20
 *      Author: hzcaoyanming
 */
#ifndef MERGE_H_
#define MERGE_H_
void mergeArray(int arr[],int begin, int mid, int end, int cacheArray[]);
void mergeSort(int arr[],int start,int end, int cacheArr[] );
void MergeSort(int arr[],int arrSize );
#endif /* MERGE_H_ */
