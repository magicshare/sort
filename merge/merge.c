/*
 * merge.c
 *
 *  Created on: 2014-8-19
 *      Author: hzcaoyanming
 */
#include <stdio.h>
#include <stdlib.h>
#include "merge.h"
//该函数用于将arr[begin,mid]与arr[mid+1,end]两个数组合按照大小关系并到cacheArry数组中
void mergeArray(int arr[],int begin, int mid, int end, int cacheArray[])
{
	int i = begin;
	int j = mid +1;

	int m = mid;
	int n = end;

	int k = 0;

	while(i <= m && j <=n )
	{
		if(arr[i] <= arr[j])
		{
			cacheArray[k++] = arr[i++];
		}
		else
		{
			cacheArray[k++] = arr[j++];
		}
	}

	while(i <= m)
	{
		cacheArray[k++] = arr[i++];
	}

	while(j <= n)
	{
		cacheArray[k++] = arr[j++];
	}

	//试想一下没有下面的代码会怎样？
	i = 0;
	while(i<k)
	{
		arr[begin+i] = cacheArray[i];
		i++;
	}
}

//采用递归的方式进行归并排序
void mergeSort(int arr[],int start,int end, int cacheArr[] )
{
	if(start<end)
	{
		int mid = (start+end)/2;
		mergeSort(arr,start,mid,cacheArr);
		mergeSort(arr,mid+1,end,cacheArr);
		mergeArray(arr,start,mid,end,cacheArr);
	}

}

//排序需要调用函数
void MergeSort(int arr[],int arrSize )
{
	int *temp = (int *)malloc(sizeof(int) * arrSize);
	mergeSort(arr, 0, arrSize-1, temp);
	free(temp);
}
