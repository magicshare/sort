/*
 * heap.c
 *
 *  Created on: 2014-8-25
 *      Author: hzcaoyanming
 */
#include "heap.h"

//从上到下调整堆
void adjustHeap(int arr[],int i,int arrLength)
{
	int j = 2*i+1;
	int temp = arr[i];

	while(j<arrLength)
	{
		//在左右孩子中找到最大的
		if(j+1<arrLength&&arr[j+1]>arr[j])
			j++;
		if(temp>arr[j])
			break;
		arr[i]=arr[j];
		i=j;
		j=2*i+1;
	}
	arr[i] = temp;
}

void Heap(int arr[],int n)
{
	 int i = n/2;
	 while(i>=0)
	 {
		 adjustHeap(arr, i, n);
		 i--;
	 }

	int j = n-1;
	while(j>0)
	{
		int temp = arr[0];
		arr[0] = arr[j];
		arr[j] = temp;
		adjustHeap(arr,0,j);
		j--;
	}
}
