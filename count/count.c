/*
 * count.c
 *
 *  Created on: 2014-8-26
 *      Author: hzcaoyanming
 */
#include <stdlib.h>
#include <stdio.h>
#include "count.h"
void Count(int arr[], int arrLength)
{
	//找出数组中的最大数与最小数
	int i = 0;
	int j = 0;
	int tmp = 0;
	int max = arr[i++];
	while(i < arrLength)
	{
		if(max < arr[i])
			max = arr[i];
		i++;
	}

	//为计数数组分为空间
	int dif = max+1;
	printf("   %d   " ,dif);
	printf("\n");

	int *count_arr = (int *)malloc(sizeof(int)* dif);
	//将计数数组清零
	i = 0;
	while(i < dif)
		count_arr[i++] = 0;
	i = 0;
	while(i < arrLength)
	{
		count_arr[arr[i]]++;
		i++;
	}
	i = 0;
	while(i < dif)
	{
		tmp = count_arr[i];
		while(tmp-->0){
			arr[j++]=i;/*i就是排序数据中的数*/
		}
		i++;
	}
	free(count_arr);
}
