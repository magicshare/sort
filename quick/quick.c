/*
 * Quick.c
 *
 *  Created on: 2014-8-20
 *      Author: hzcaoyanming
 */
#include <stdio.h>
#include <stdlib.h>
#include "quick.h"

void QuickSort(int arr[],int left,int right)
{
	if(left < right)
	{
		int i = left , j =  right, x = arr[left];
		while(i < j)
		{
			while(i < j && arr[j] >= x )
				j--;
			if(i  <  j)
				arr[i++] = arr[j];

			while(i <j && arr[i] < x )
				i++;
			if(i < j)
				arr[j--] = arr[i];
		}

		arr[i] = x;
		QuickSort(arr, left, i-1);
		QuickSort(arr, i+1, right);
	}
}



