/*
 * print.c
 *
 *  Created on: 2014-8-20
 *      Author: hzcaoyanming
 */
#include <stdio.h>
#include <stdlib.h>
#include "merge/merge.h"
#include "quick/quick.h"
#include "insertion/insertion.h"
#include "select/select.h"
#include "heap/heap.h"
#include "count/count.h"
//打印数组
void printArr(int arr[],int size )
{
	int i = 0;
	while(i<size)
	{
		printf("%d    ",arr[i++]);
	}
}

int main(int argc, char **argv)
{
	int a[] = {3,1,55,34,23,56,79,2,33,4,23,67,2};
	int arrSize = sizeof(a)/sizeof(int);

	/*归并排序
	printf("%s    \n","归并排序前：");
	printArr(a,arrSize);
	printf("\n");
	MergeSort(a,arrSize);
	printf("%s    \n","归并排序后：");
	printArr(a,arrSize);
	printf("\n");
	*/

	/*快速排序
	printf("%s    \n","快速排序前：");
	printArr(a,arrSize);
	printf("\n");
	QuickSort(a,0,arrSize-1);
	printf("%s    \n","快速排序后：");
	printArr(a,arrSize);
	printf("\n");
	 */

	/*直接插入排序
	printf("%s    \n","直接插入排序前：");
	printArr(a,arrSize);
	printf("\n");
	Insertion(a,arrSize);
	printf("%s    \n","直接插入排序后：");
	printArr(a,arrSize);
	printf("\n");
	 */

	/*直接选择排序
	printf("%s    \n","选择插入排序前：");
	printArr(a,arrSize);
	printf("\n");
	Select(a,arrSize);
	printf("%s    \n","选择插入排序后：");
	printArr(a,arrSize);
	printf("\n");
	return 0;
	*/

	/*堆排序
	printf("%s    \n","堆排序前：");
	printArr(a,arrSize);
	printf("\n");
	Heap(a,arrSize);
	printf("%s    \n","堆排序后：");
	printArr(a,arrSize);
	printf("\n");
	*/

	//计数排序
	printf("%s    \n","计数排序前：");
	printArr(a,arrSize);
	printf("\n");
	Count(a,arrSize);
	printf("%s    \n","计数排序后：");
	printArr(a,arrSize);
	printf("\n");
	return 0;
}
